import logging
import datetime
import argparse
from collections import namedtuple
import os 
import typing 
import xml.etree.ElementTree as ET
import csv
import sys

Dir = namedtuple('Dir', 'path files')
LogFile = namedtuple('log_file', 'filename date')

def read_argument():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_file", help="file xml")
    return parser.parse_args()


def working_dir(xml_file: str) -> typing.NamedTuple:
    path = None
    files = None 
    if os.path.exists(xml_file):
        files = os.path.abspath(xml_file)
        path = os.path.dirname(files)
    dir = Dir(path, files)
    return dir


def dir_log(work_path: str) -> str:
    dir_log = os.path.join(work_path, 'log')
    if not os.path.exists(dir_log):
        os.mkdir(dir_log)
    return dir_log
    

def read_xml(file_xml: str) -> dict:
    dict_payment = {}
    remove_key = []
    try:
        tree = ET.ElementTree(file=file_xml)
    except ET.ParseError:
        logging.debug("xml file is not the correct structure or is damaged")
        return False    
    root = tree.getroot()
    for attr_doc in  root.iter('ИдФайл'):
        date_doc =  attr_doc.find('ДатаФайл').text
        break
    try: 
        datetime.datetime.strptime(date_doc, "%d.%m.%Y")
    except ValueError:
        logging.info("invalid date format ")
        return False    

    for attr_payment in root.iter('Плательщик'):
        licch = attr_payment.find('ЛицСч').text
        fio = attr_payment.find('ФИО').text
        address = attr_payment.find('Адрес').text
        period = attr_payment.find('Период').text
        price = attr_payment.find('Сумма').text
        if dict_payment.get(f'{licch}_{period}'):
            logging.info(f"{licch}_{period} key repetitions")
            remove_key.append(licch + '_' + period)
            continue
        dict_payment[f'{licch}_{period}'] = [date_doc, licch, fio, address, period, price]
    
    for key in remove_key:
        try:
            dict_payment.pop(key, None)
        except KeyError:
            logging.info(f'clear dict, the key: {key} has already been deleted ')
    return dict_payment


def check_date(str_date: str) -> bool:
    try:
        if len(str_date) == 6:
            datetime.datetime.strptime(str_date, "%m%Y")
            return True
    except (ValueError, TypeError):
        logging.info(f"date validation, invalid date type and value  {str_date}")
    return False


def check_price(price: str) -> bool:
    price_good = False
    try:
        if float(price):
            if len(str(float(price)).split('.')[1]) == 2 and float(price) > 0:
                price_good = True
    except (ValueError, AttributeError):
        logging.info(f"checking price value, not correct format  {price}")
    return price_good
        

def clear_data(data_dict : dict) -> dict:
    clear_dict = {}
    for key_data in data_dict:
        if not check_price(data_dict[key_data][5]):
            logging.info(f'{key_data} not correct format sum: {data_dict[key_data][5]}')
            continue
        if not check_date(data_dict[key_data][4]):
            logging.info(f'{key_data} not correct date: {data_dict[key_data][4]}')
            continue
        if not data_dict[key_data][1]:
            logging.info(f'{key_data}  not personal account: {data_dict[key_data][1]}')
            continue
        clear_dict[key_data] = data_dict[key_data]
    return clear_dict


def write_csv(dict_date: dict, file_name: str) -> None:
    file_name = file_name.replace('.xml', '.csv')
    try:
        with open(file_name, 'w', encoding='windows-1251') as csv_file:
            write = csv.writer(csv_file, delimiter=';', lineterminator="\r")
            for key in dict_date:
                write.writerow([os.path.basename(file_name)] + dict_date[key])
        logging.info("success write csv file")
    except csv.Error as e:
        logging.info(f'error write csv: {e}')


def archive_xml(file_xml: str, work_path: str) -> None:
    dir_arh = os.path.join(work_path, 'arh')
    if not os.path.exists(dir_arh):
        logging.info("create folder arh")
        os.mkdir(dir_arh)
    try:
        os.replace(file_xml, os.path.join(dir_arh, os.path.basename(file_xml)))
    except Exception as e:
        logging.info(f" ошибка создания файла : {os.path.join(dir_arh, os.path.basename(file_xml))} error: {e}")

    logging.info(f'succes copy file {file_xml} to {dir_arh}')


def main():
    arguments = read_argument().input_file
    if not arguments:
        return 
    work_dir = working_dir(arguments)
    if not work_dir.path:
        print("Путь или файл xml указан не верно")
        return
    log_dir = dir_log(work_dir.path)
    
    logging.basicConfig(filename=os.path.join(log_dir, 'log_xml_parser.log'),
                        level=logging.INFO,
                        format='[%(asctime)s] %(levelname)s %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S')
    logging.info("----START----")
    
    data_xml = read_xml(work_dir.files)
    
    if not data_xml:
        return
    
    dict_for_csv = clear_data(data_xml)
    write_csv(dict_for_csv, work_dir.files)   
    archive_xml(work_dir.files, work_dir.path)
    logging.info("+++END+++")


if __name__ == "__main__":
    try:
        main()
    except SystemExit:
        logging.exception("exit")
    except Exception as e:
        logging.exception("Unexpected error: %s" % (e))
